package vocalabjad;

import java.util.*;
import java.util.stream.Stream;

import static java.lang.System.*;


public class PrintVocalAbjad {
    public static void main(String[] args) {
        String str = "bayu";

        StringBuilder builder = new StringBuilder(str);
        HashSet<String> mySet = new HashSet<>();

        for (int i = 0; i < builder.length(); i++) {
            mySet.add(builder.charAt(i) + "");
        }
        int counter = 0;
        StringBuilder result = new StringBuilder();
        for (String s : mySet)
            if (Stream.of("a", "i", "u", "e", "o").anyMatch(s::equalsIgnoreCase)) {
                if (counter == 0) result.append(s);
                else result.append(" dan ").append(s);
                counter++;

            }
        out.print(counter);
        out.print(" yaitu ");
        out.print(result);

    }

}











